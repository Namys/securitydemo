<%--
  Created by IntelliJ IDEA.
  User: Namys
  Date: 05.10.2016
  Time: 17:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <link href="webjars/bootstrap/3.3.4/css/bootstrap.css"
          rel="stylesheet" media="screen"/>
    <link href="signin.css"
          rel="stylesheet" media="screen"/>
    <script src="webjars/jquery/2.1.4/jquery.min.js"></script>
    <title></title>
</head>
<body>
<sec:authorize access="hasRole('ROLE_ADMIN')">
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <div class="navbar-brand">Библиотека</div>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="/">Начальная Страница</a></li>
                    <li><a href="admin/books">Книги</a></li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>
</sec:authorize>
<form name="loginForm" action="<c:url value="/"/>" method="post" class="form-signin">
    <div class="crop"><img src="lock.jpg" class="lock form-signin-heading"></div>
    <sec:authorize access="hasRole('ROLE_ADMIN')">
        <style>
            .lock {
                left: -34px;
            }
        </style>
    </sec:authorize>
    <c:if test="${not empty pageContext.request.getAttribute('username')}">
        <div>Welcome : ${pageContext.request.getAttribute('username')}</div>
    </c:if>
    <c:if test="${not empty pageContext.request.getAttribute('error')}">
        <div style="color: red"> ${pageContext.request.getAttribute('error')}</div>
    </c:if>
    <input name="login" type="text" title="login" class="form-control" placeholder="Введите логин">
    <input name="password" type="password" title="password" class="form-control" placeholder="Введите пароль">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <input type="submit" class="btn btn-lg btn-success btn-block">
</form>
</body>
</html>
