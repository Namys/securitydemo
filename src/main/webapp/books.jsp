<%--
  Created by IntelliJ IDEA.
  User: Namys
  Date: 14.10.2016
  Time: 10:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <link href="/webjars/bootstrap/3.3.4/css/bootstrap.css"
          rel="stylesheet" media="screen"/>
    <script src="/webjars/jquery/2.1.4/jquery.min.js"></script>
    <script src="/webjars/bootstrap/3.3.4/js/bootstrap.js"></script>
    <style>
        body {
            padding-top: 50px;
        }

        .panel-default {
            padding: 40px 15px;
            text-align: center;
        }
        .panel-heading{
            text-align: left;
        }
        .panel-body{
            text-align: left;
        }
    </style>
    <title></title>
</head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <div class="navbar-brand">Библиотека</div>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/">Начальная Страница</a></li>
                <li><a href="books">Книги</a></li>
                <li><a href="#myModal" role="button" class="btn" data-toggle="modal">Добавить книгу.</a></li>
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
</div>
<div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading">
        <div>Список книг.</div>
    </div>
    <div class="panel-body">
        <form action="books" method="get" enctype="text/plain">
            <label for="FName">Название</label>
            <input name="name" id="FName" type="text" value="${name}">
            <label for="FAuthor">Автор</label>
            <input name="author" id="FAuthor" type="text" value="${author}">
            <input type="submit" value="Фильтровать">
        </form>
    </div>
    <!-- Table -->
    <table class="table">
        <tr>
            <th>Название</th>
            <th>Автор</th>
            <th>Подробнее</th>
            <th></th>
        </tr>
        <c:forEach items="${books}" var="book">
            <tr>
                <td>${book.name}</td>
                <td>${book.author}</td>
                <td><a href="<c:url value="book?id=${book.id}"/>">Посмотреть/изменить</a></td>
                <td><a href="<c:url value="delBook?id=${book.id}"/>">Удалить</a></td>
            </tr>
        </c:forEach>
    </table>
</div>
<div class="container">
    <ul class="pager" style="float: left">
        <c:if test="${(pageNumber)>0}"><li class="previous"><a href="books?name=${name}&author=${author}&pageNumber=${pageNumber-1}&limit=${limit}">Previous</a></li></c:if>
        <c:if test="${(pageNumber+1)<max}"><li class="next"><a href="books?name=${name}&author=${author}&pageNumber=${pageNumber+1}&limit=${limit}">Next</a></li></c:if>
    </ul>
</div>
<!-- Large modal -->
<div id="myModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<c:url value="saveBook"/>" method="post" enctype="multipart/form-data">
                <input type="file" name="file" class="form-control">
                <label for="name">Название</label>
                <input type="text" name="name" id="name" class="form-control"/>
                <label for="author">Автор</label>
                <input type="text" name="author" id="author" class="form-control"/>
                <label for="description">Описание</label>
                <textarea name="description" id="description" class="form-control"></textarea>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <input type="submit">
            </form>
        </div>
    </div>
</div>
</body>
</html>
