<%--
  Created by IntelliJ IDEA.
  User: Namys
  Date: 14.10.2016
  Time: 12:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <link href="/webjars/bootstrap/3.3.4/css/bootstrap.css"
          rel="stylesheet" media="screen"/>
    <script src="/webjars/jquery/2.1.4/jquery.min.js"></script>
    <script src="/webjars/bootstrap/3.3.4/js/bootstrap.js"></script>
    <title></title>
    <style>
        body {
            padding-top: 50px;
        }

        .starter-template {
            padding: 40px 15px;
            text-align: center;
        }
    </style>
</head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <div class="navbar-brand">Библиотека</div>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/">Начальная Страница</a></li>
                <li><a href="books">Книги</a></li>
                <li><a href="#myModal" role="button" class="btn" data-toggle="modal">Изменить описание книги.</a></li>
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
</div>
<div class="container">
    <div class="starter-template" style="text-align: left">
        <c:if test="${not empty img}">
            <img src="${img}" width="250">
        </c:if>
        <c:if test="${empty img}">
            <img src="/no_picture.jpg" width="250">
        </c:if>
        <h1>Название:${book.name}. Автор:${book.author}.</h1>

        <div>Описание:</div>
        <p class="lead">${book.description}</p>
    </div>
</div>
<div id="myModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="<c:url value="saveBook"/>" method="post" enctype="multipart/form-data">
                <input type="file" name="file" class="form-control">
                <input type="hidden" name="id" value="${book.id}">
                <label for="name">Название</label>
                <input type="text" name="name" id="name" value="${book.name}" class="form-control"/>
                <label for="author">Автор</label>
                <input type="text" name="author" id="author" value="${book.author}" class="form-control"/>
                <label for="description">Описание</label>
                <textarea name="description" id="description" class="form-control">${book.description}</textarea>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <input type="submit">
            </form>
        </div>
    </div>
</div>
</body>
</html>
