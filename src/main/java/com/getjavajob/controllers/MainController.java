package com.getjavajob.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Namys on 05.10.2016.
 */
@Controller
public class MainController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView getIndex(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView("index");
        String username = (String) request.getAttribute("username");
        if (username != null) {
            modelAndView.addObject("msg", "Welcome : " + username);
        }
        return modelAndView;
    }

    @RequestMapping(value = "/admin/", method = RequestMethod.GET)
    public ModelAndView adminPage() {
        ModelAndView model = new ModelAndView("admin");
        model.addObject("title", "Spring Security + Hibernate Example");
        model.addObject("message", "This page is for ROLE_ADMIN only!");
        return model;
    }
}
