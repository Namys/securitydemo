package com.getjavajob.controllers;

import com.getjavajob.model.Book;
import com.getjavajob.service.BookService;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;

/**
 * Created by Namys on 14.10.2016.
 */
@Controller()
@RequestMapping("/admin")
public class BookController {
    @Autowired
    BookService service;

    @RequestMapping(value = "/books", method = RequestMethod.GET)
    public ModelAndView getBooks(@RequestParam(value = "name", defaultValue = "", required = false) String name, @RequestParam(value = "author", defaultValue = "", required = false) String author, @RequestParam(required = false, defaultValue = "0", name = "pageNumber") int pageNumber, @RequestParam(required = false, defaultValue = "10", name = "limit") int limit) {
        ModelAndView modelAndView = new ModelAndView("books");
        Page<Book> page;
        if (name.isEmpty() && author.isEmpty()) {
            page = service.getAll(pageNumber, limit);
        } else if (!name.isEmpty() && !author.isEmpty()) {
            page = service.getByNameAndAuthor(name, author, pageNumber, limit);
        } else {
            if (name.isEmpty()) {
                page = service.getByNameOrAuthor(null, author, pageNumber, limit);
            } else {
                page = service.getByNameOrAuthor(name, null, pageNumber, limit);
            }

        }
        if (page != null) {
            modelAndView.addObject("books", page.getContent());
            modelAndView.addObject("max", page.getTotalPages());
        }
        modelAndView.addObject("name", name);
        modelAndView.addObject("author", author);
        modelAndView.addObject("pageNumber", pageNumber);
        modelAndView.addObject("limit", limit);
        return modelAndView;
    }

    @RequestMapping(value = "/book", method = RequestMethod.GET)
    public ModelAndView getBook(@RequestParam(value = "id", required = false) Integer id) {
        ModelAndView modelAndView = new ModelAndView("book");
        Book book = service.get(id);
        modelAndView.addObject("book", book);
        if (book.getImg() != null && book.getImg().length != 0)
            modelAndView.addObject("img", "data:image/png;base64," + Base64.encode(book.getImg()));
        return modelAndView;
    }

    @RequestMapping(value = "/saveBook", method = RequestMethod.POST)
    public String saveBook(Book book, @RequestParam("file") MultipartFile file) {
        byte[] bytes = new byte[0];
        try {
            bytes = file.getBytes();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (bytes.length == 0 && (book.getId() != null && book.getId() != 0)) {
            bytes = service.get(book.getId()).getImg();
        }
        book.setImg(bytes);
        book = service.save(book);
        return "redirect:book?id=" + book.getId();
    }

    @RequestMapping(value = "/delBook", method = RequestMethod.GET)
    public String delBook(@RequestParam(value = "id", required = false) Integer id) {
        service.delete(id);
        return "redirect:books";
    }
}
