package com.getjavajob.service;

import com.getjavajob.dao.BookDao;
import com.getjavajob.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by Namys on 14.10.2016.
 */
@Service
@Transactional
public class BookService {
    @Autowired
    private BookDao dao;

    public Book save(Book book){
        return dao.save(book);
    }
    public void delete(int id){
        dao.delete(id);
    }
    public Page<Book> getAll(int pageNumber, int limit){
        return dao.findAll(new PageRequest(pageNumber,limit));
    }
    public Book get(int id){
        return dao.findOne(id);
    }
    public Page<Book> getByNameAndAuthor(String name,String author,int pageNumber,int limit){
        return dao.findByNameAndAuthor(name, author, new PageRequest(pageNumber,limit));
    }


    public Page<Book> getByNameOrAuthor(String name,String author,int offset,int limit){
        return dao.findByNameOrAuthor(name, author, new PageRequest(offset, limit));
    }
}
