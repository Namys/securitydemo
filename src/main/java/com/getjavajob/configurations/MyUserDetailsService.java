package com.getjavajob.configurations;

import com.getjavajob.dao.ClientDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * Created by Namys on 04.10.2016.
 */
@Service("userDetailsService")
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private ClientDao clientDao;

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(final String login) throws UsernameNotFoundException{
        List<? extends GrantedAuthority> role_admin = Collections.singletonList(() -> "ROLE_ADMIN");
        com.getjavajob.model.Client client = clientDao.findByLogin(login);
        if (client==null){
            throw new UsernameNotFoundException("user not found");
        }
        String psw=null;
        try {
            psw = getString(client.getId());
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (psw==null){
            throw new UsernameNotFoundException("password not found");
        }
        return new User(client.getLogin(),psw,role_admin);
    }

    private String getString(int id) throws IOException {
        BufferedReader reader=new BufferedReader(new FileReader("src/main/resources/authorization.txt"));
        boolean search=true;
        String psw = null;
        while (reader.ready()&&search){
            String[] split = reader.readLine().split("\t");
            if (split[0].equals(String.valueOf(id))){
                psw=split[1];
                search=false;
            }
        }
        return psw;
    }
}
