package com.getjavajob.configurations;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Namys on 06.10.2016.
 */
@Component
public class MyAuthenticationFailureHandler implements AuthenticationFailureHandler {
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        if(exception.getClass() == UsernameNotFoundException.class) {
            request.setAttribute("error","cannot find a user");
        } else if(exception.getClass() == BadCredentialsException.class) {
            request.setAttribute("error","check your password");
        }
//        request.setAttribute("error", (UsernameNotFoundException)exception);
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
}
