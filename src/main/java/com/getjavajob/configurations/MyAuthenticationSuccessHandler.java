package com.getjavajob.configurations;

import com.getjavajob.dao.ClientDao;
import com.getjavajob.model.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Namys on 05.10.2016.
 */
@Component
public class MyAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    @Autowired
    private ClientDao clientDao;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        User login = (User) authentication.getPrincipal();
        Client client = clientDao.findByLogin(login.getUsername());
        request.setAttribute("username", client.getName());
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
}
