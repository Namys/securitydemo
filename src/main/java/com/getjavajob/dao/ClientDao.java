package com.getjavajob.dao;

import com.getjavajob.model.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

/**
 * Created by Namys on 04.10.2016.
 */
@Repository
public class ClientDao {

    @Autowired
    EntityManager manager;

    @SuppressWarnings("JpaQlInspection")
    public Client findByLogin(String login) {
        try {
            return manager.createQuery("select t FROM client t where t.login = :value1",Client.class).setParameter("value1", login).getSingleResult();
        }catch (Exception e){
            return null;
        }
    }

    public Client findById(int id){
        return manager.find(Client.class,id);
    }
}
