package com.getjavajob.dao;

import com.getjavajob.model.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;


/**
 * Created by Namys on 14.10.2016.
 */
public interface BookDao extends PagingAndSortingRepository<Book,Integer> {
    Page<Book> findByNameAndAuthor(String name,String author,Pageable pageable);

    Page<Book> findByNameOrAuthor(String name, String author,Pageable pageable);
}
