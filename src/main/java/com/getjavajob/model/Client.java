package com.getjavajob.model;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by Namys on 04.10.2016.
 */
@Entity(name = "client")
public class Client {

    @Id
    private int id;
    private String login;
    private String name;

    public Client() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
