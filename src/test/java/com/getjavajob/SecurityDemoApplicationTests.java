package com.getjavajob;

import org.junit.Assert;
import org.junit.Test;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

//@RunWith(SpringRunner.class)
@SpringBootTest

public class SecurityDemoApplicationTests {

    //	/*@Autowired
//	ClientDao clientDao;
    @Test
    public void contextLoads() {
//		Assert.assertEquals(1, clientDao.findById(1).getId());
//		Assert.assertEquals("nick", clientDao.findByLogin("login2").getName());
        final org.springframework.security.crypto.password.PasswordEncoder delegate = new BCryptPasswordEncoder();
        PasswordEncoder passwordEncoder = new PasswordEncoder() {
            public String encodePassword(String rawPass, Object salt) {
                checkSalt(salt);
                return delegate.encode(rawPass);
            }

            public boolean isPasswordValid(String encPass, String rawPass, Object salt) {
                checkSalt(salt);
                return delegate.matches(rawPass, encPass);
            }

            private void checkSalt(Object salt) {
            }
        };
        boolean passwordValid = passwordEncoder.isPasswordValid("$2a$06$0gPtJx/YtSI0bcjrr.Q.4O8RR4UojbNP3JKq3iKgWGZAxaCXoqgdu", "password", new Object());
//		boolean passwordValid = passwordEncoder.isPasswordValid("$2a$08$1pvUALDO/2y.EmJU2uoNWOr46/uUKSgPOoHh/wxUYY5QbzdBuUoay","1234", new Object());
        Assert.assertEquals(true, passwordValid);
    }

}
